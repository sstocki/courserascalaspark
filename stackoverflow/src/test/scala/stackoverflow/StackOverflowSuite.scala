package stackoverflow

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {

  val conf: SparkConf = new SparkConf().setMaster("local").setAppName("StackOverflow")
  val sc: SparkContext = new SparkContext(conf)

  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

    override def langSpread = 50000

    override def kmeansKernels = 30

    override def kmeansEta: Double = 20.0D

    override def kmeansMaxIterations = 120
  }

  val lines: RDD[String] = sc.parallelize(
    List(
      "1,16646028,,,2,PHP",
      "2,16646093,,16646028,0,",
      "2,16646099,,16646028,0,",
      "2,16646100,,16646028,1,",
      "2,16646103,,16646028,1,",
      "2,16646108,,16646028,2,",
      "2,16646113,,16646028,3,",
      "2,16646173,,16646028,5,",
      "2,16646271,,16646028,0,",
      "1,17715277,,,2,C#",
      "2,17715437,,17715277,0,",
      "2,17715463,,17715277,2,",
      "1,18715233,,,2,GoLang",
      "2,18715234,,18715233,0,",
      "2,18715235,,18715233,4,")
  )

  val postings: List[Posting] = List(
    Posting(1, 16646028, None, None, 2, Some("PHP")),
    Posting(2, 16646093, None, Some(16646028), 0, None),
    Posting(2, 16646099, None, Some(16646028), 0, None),
    Posting(2, 16646100, None, Some(16646028), 1, None),
    Posting(2, 16646103, None, Some(16646028), 1, None),
    Posting(2, 16646108, None, Some(16646028), 2, None),
    Posting(2, 16646113, None, Some(16646028), 3, None),
    Posting(2, 16646173, None, Some(16646028), 5, None),
    Posting(2, 16646271, None, Some(16646028), 0, None),
    Posting(1, 17715277, None, None, 2, Some("C#")),
    Posting(2, 17715437, None, Some(17715277), 0, None),
    Posting(2, 17715463, None, Some(17715277), 2, None),
    Posting(1, 18715233, None, None, 2, Some("GoLang")),
    Posting(2, 18715234, None, Some(18715233), 0, None),
    Posting(2, 18715235, None, Some(18715233), 4, None)
  )

  val firstQ: Posting = postings.head

  val secondQ: Posting = postings(9)

  val thirdQ: Posting = postings(12)

  val postingsRDD: RDD[Posting] = sc.parallelize(postings)

  val scoredPostings: List[(Posting, Int)] = List(
    (firstQ, 5),
    (secondQ, 2),
    (thirdQ, 4)
  )

  val scoredPostingsRDD: RDD[(Posting, Int)] = sc.parallelize(scoredPostings)

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  test("rawPostings") {
    val expectedPostings = postings
    val actualPostings: List[Posting] = testObject.rawPostings(lines).collect().toList
    assert(expectedPostings.sortBy(_.id) === actualPostings.sortBy(_.id), "Error creating rawPostings")
  }

  test("groupedPostings") {
    val postingsList: Array[Posting] = postings.toArray
    val expectedGrouped = List(
      (firstQ.id, List(
        (firstQ, postingsList(1)),
        (firstQ, postingsList(2)),
        (firstQ, postingsList(3)),
        (firstQ, postingsList(4)),
        (firstQ, postingsList(5)),
        (firstQ, postingsList(6)),
        (firstQ, postingsList(7)),
        (firstQ, postingsList(8))
      )),
      (secondQ.id, List(
        (secondQ, postingsList(10)),
        (secondQ, postingsList(11))
      )),
      (thirdQ.id, List(
        (thirdQ, postingsList(13)),
        (thirdQ, postingsList(14))
      ))
    )
    val actualGrouped: List[(Int, Iterable[(Posting, Posting)])] =
      testObject.groupedPostings(postingsRDD).collect().toList.map(t => (t._1, t._2.toList))

    assert(expectedGrouped === actualGrouped, "Error creating groupedPostings")
  }

  test("scoredPostings") {
    val expectedScoredPostings = scoredPostings
    val input: RDD[(Int, Iterable[(Posting, Posting)])] = testObject.groupedPostings(postingsRDD)
    val actualScoredPostings: List[(Posting, Int)] = testObject.scoredPostings(input).collect().toList

    assert(expectedScoredPostings === actualScoredPostings, "Error creating scoredPostings")
  }

  test("vectorPostings") {
    val expectedVectorPostings: List[(Int, Int)] = List(
      (testObject.langs.indexOf(firstQ.tags.get) * testObject.langSpread, 5),
      (testObject.langs.indexOf(secondQ.tags.get) * testObject.langSpread, 2)
    )
    val actualVectorPostings = testObject.vectorPostings(scoredPostingsRDD).collect().toList
    assert(expectedVectorPostings === actualVectorPostings, "Error creating vectorPostings")

  }

  test("kmeans") {
    val sampleMultipleTime = false
    val percentOfFullRDD = 0.1
    val lines = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv").sample(sampleMultipleTime, percentOfFullRDD)
    val raw: RDD[Posting] = testObject.rawPostings(lines)
    val grouped: RDD[(Int, Iterable[(Posting, Posting)])] = testObject.groupedPostings(raw)
    val scored: RDD[(Posting, Int)] = testObject.scoredPostings(grouped)
    val vectors: RDD[(Int, Int)] = testObject.vectorPostings(scored).cache()

    val means: Array[(Int, Int)] = testObject.kmeans(testObject.sampleVectors(vectors), vectors)
    val results = testObject.clusterResults(means, vectors)
    testObject.printResults(results)
  }
}
