package timeusage

import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{DoubleType, StringType}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSpec}
import timeusage.TimeUsage._

@RunWith(classOf[JUnitRunner])
class TimeUsageSuite extends FunSpec with BeforeAndAfterAll {

  val columnNames: List[String] = TimeUsage.spark.sparkContext.textFile(fsPath("/timeusage/atussum.csv")).first().split(",").to[List]

  describe("dfSchema") {

    it("should return the schema of the DataFrame") {
      val actual = dfSchema(columnNames)
      assert(actual.fields.head.dataType === StringType)
      for(field <- actual.fields.tail) assert(field.dataType === DoubleType)
    }
  }

  describe("classifiedColumns") {

    it("should return ") {
      val (actualPrimaryNeeds, actualWorkingActivities, actualOther) = classifiedColumns(columnNames)
      val expectedPrimaryNeeds = List(col("t110199"), col("t030399"), col("t030402"), col("t110281"), col("t119999"),
        col("t030401"), col("t030109"), col("t010102"), col("t030201"), col("t030405"), col("t039999"), col("t010401"),
        col("t030299"), col("t180101"), col("t030110"), col("t010301"), col("t030103"), col("t030203"), col("t180399"),
        col("t010299"), col("t030199"), col("t010399"), col("t030101"), col("t030502"), col("t010499"), col("t030599"),
        col("t030303"), col("t019999"), col("t180382"), col("t030108"), col("t030503"), col("t030403"), col("t030104"),
        col("t030186"), col("t010501"), col("t030112"), col("t030302"), col("t030105"), col("t030202"), col("t030501"),
        col("t180381"), col("t030504"), col("t030301"), col("t030404"), col("t110289"), col("t010101"), col("t010201"),
        col("t030499"), col("t110101"), col("t030204"), col("t030102"), col("t030111"), col("t180199"), col("t010199"),
        col("t010599"))
      val expectedWorkingActivities = List(col("t050203"), col("t050301"), col("t050404"), col("t050302"),
        col("t180589"), col("t050289"), col("t050304"), col("t050202"), col("t050201"), col("t050403"), col("t180502"),
        col("t050102"), col("t050303"), col("t050101"), col("t050103"), col("t059999"), col("t050204"), col("t050481"),
        col("t180501"), col("t050499"), col("t050405"), col("t050189"), col("t050389"))
      val expectedOther = List(col("t070299"), col("t020899"), col("t120313"), col("t130230"), col("t130208"),
        col("t080699"), col("t160106"), col("t020302"), col("t180804"), col("t181801"), col("t020399"), col("t080601"),
        col("t090201"), col("t040299"), col("t120302"), col("t080199"), col("t020401"), col("t181299"), col("t109999"),
        col("t080399"), col("t130215"), col("t130116"), col("t180901"), col("t090102"), col("t130202"), col("t180803"),
        col("t130225"), col("t130216"), col("t160103"), col("t040499"), col("t130220"), col("t150199"), col("t139999"),
        col("t180807"), col("t020499"), col("t140101"), col("t040501"), col("t120306"), col("t120303"), col("t181204"),
        col("t040401"), col("t100101"), col("t120401"), col("t040201"), col("t040507"), col("t130223"), col("t130124"),
        col("t150402"), col("t100383"), col("t090299"), col("t080599"), col("t100201"), col("t180280"), col("t099999"),
        col("t129999"), col("t040402"), col("t150203"), col("t160107"), col("t130135"), col("t180801"), col("t100399"),
        col("t180601"), col("t130122"), col("t040108"), col("t130128"), col("t090302"), col("t130211"), col("t120502"),
        col("t150301"), col("t090301"), col("t130109"), col("t180904"), col("t060399"), col("t160104"), col("t130123"),
        col("t149999"), col("t020799"), col("t090501"), col("t090103"), col("t080502"), col("t100102"), col("t040110"),
        col("t181399"), col("t090502"), col("t120311"), col("t180902"), col("t120312"), col("t180701"), col("t020681"),
        col("t181599"), col("t080799"), col("t150599"), col("t060199"), col("t049999"), col("t020801"), col("t130222"),
        col("t120101"), col("t080801"), col("t150201"), col("t120305"), col("t020901"), col("t090499"), col("t090101"),
        col("t040399"), col("t120504"), col("t070105"), col("t130203"), col("t020101"), col("t150104"), col("t060103"),
        col("t020902"), col("t040504"), col("t181283"), col("t060402"), col("t060102"), col("t040403"), col("t020103"),
        col("t150401"), col("t130226"), col("t020303"), col("t120402"), col("t130499"), col("t181899"), col("t130228"),
        col("t040199"), col("t130107"), col("t090402"), col("t070199"), col("t180481"), col("t020301"), col("t120403"),
        col("t130401"), col("t130115"), col("t150204"), col("t130227"), col("t120201"), col("t130134"), col("t130205"),
        col("t060289"), col("t180805"), col("t020599"), col("t080403"), col("t189999"), col("t130219"), col("t060201"),
        col("t080402"), col("t079999"), col("t140103"), col("t020201"), col("t130111"), col("t160102"), col("t130110"),
        col("t160101"), col("t130130"), col("t040405"), col("t130121"), col("t130229"), col("t040102"), col("t080202"),
        col("t130210"), col("t020203"), col("t080602"), col("t080302"), col("t020999"), col("t130217"), col("t100199"),
        col("t040301"), col("t130204"), col("t040204"), col("t080701"), col("t040111"), col("t040186"), col("t181101"),
        col("t120310"), col("t180999"), col("t070301"), col("t060403"), col("t181301"), col("t120503"), col("t120299"),
        col("t040508"), col("t130117"), col("t080201"), col("t180682"), col("t130120"), col("t100299"), col("t130201"),
        col("t040502"), col("t130221"), col("t080203"), col("t180905"), col("t100381"), col("t150105"), col("t040506"),
        col("t130127"), col("t040109"), col("t040105"), col("t120404"), col("t169989"), col("t020903"), col("t180903"),
        col("t160105"), col("t120308"), col("t070103"), col("t150699"), col("t130232"), col("t140105"), col("t080899"),
        col("t130213"), col("t080401"), col("t150106"), col("t100401"), col("t040104"), col("t040112"), col("t130218"),
        col("t130399"), col("t130113"), col("t130104"), col("t150601"), col("t150102"), col("t020904"), col("t020699"),
        col("t089999"), col("t130119"), col("t060101"), col("t090401"), col("t090599"), col("t130402"), col("t020199"),
        col("t130118"), col("t020701"), col("t120499"), col("t029999"), col("t090202"), col("t080301"), col("t020501"),
        col("t181201"), col("t150101"), col("t130199"), col("t181601"), col("t070104"), col("t040202"), col("t090104"),
        col("t060104"), col("t060202"), col("t120307"), col("t181501"), col("t150299"), col("t150399"), col("t140104"),
        col("t150302"), col("t130126"), col("t120309"), col("t080501"), col("t130206"), col("t080102"), col("t090399"),
        col("t140102"), col("t020299"), col("t150202"), col("t020202"), col("t180802"), col("t060302"), col("t069999"),
        col("t130133"), col("t130129"), col("t020905"), col("t181699"), col("t180482"), col("t040101"), col("t181002"),
        col("t180699"), col("t130214"), col("t181202"), col("t130103"), col("t130125"), col("t120304"), col("t130136"),
        col("t040599"), col("t080499"), col("t040203"), col("t070101"), col("t020104"), col("t181302"), col("t130106"),
        col("t040404"), col("t060401"), col("t060203"), col("t120301"), col("t040103"), col("t070399"), col("t130224"),
        col("t130212"), col("t020102"), col("t180806"), col("t130301"), col("t181401"), col("t130132"), col("t130231"),
        col("t060303"), col("t150103"), col("t040303"), col("t180782"), col("t070102"), col("t180499"), col("t180899"),
        col("t020402"), col("t130102"), col("t130105"), col("t160108"), col("t080299"), col("t120501"), col("t060301"),
        col("t120399"), col("t130207"), col("t020502"), col("t080702"), col("t040302"), col("t181499"), col("t130299"),
        col("t181099"), col("t120202"), col("t090199"), col("t120199"), col("t040503"), col("t181199"), col("t150501"),
        col("t070201"), col("t130131"), col("t130302"), col("t150499"), col("t130209"), col("t150602"), col("t181081"),
        col("t159989"), col("t040505"), col("t060499"), col("t130114"), col("t120599"), col("t080101"), col("t120405"),
        col("t130112"), col("t100103"), col("t130101"), col("t100499"), col("t130108"))
      assert(actualPrimaryNeeds.toSet === expectedPrimaryNeeds.toSet)
      assert(actualWorkingActivities.toSet === expectedWorkingActivities.toSet)
      assert(actualOther.toSet === expectedOther.toSet)
    }
  }

  describe("timeUsage and timeUsageSql and timeUsageTyped") {

    it("should return same results") {

      def rowToRoundedString(rows: Array[Row]): Array[String] = {
        rows.map(r =>
        "[" + r.getAs[String](0) + ";" + r.getAs[Double](1).round.toString + ";" + r.getAs[Double](2).round.toString
          + ";" + r.getAs[Double](3).round.toString + ";" + r.getAs[Double](4).round.toString
          + ";" + r.getAs[Double](5).round.toString)
      }

      val (columns, initDf) = read("/timeusage/atussum.csv")
      val (primaryNeedsColumns, workColumns, otherColumns) = classifiedColumns(columns)
      val summaryDf = timeUsageSummary(primaryNeedsColumns, workColumns, otherColumns, initDf)
      val finalDf = timeUsageGrouped(summaryDf).rdd.collect().map(_.toString()).toSet
      val finalDfSql = timeUsageGroupedSql(summaryDf).rdd.collect().map(_.toString()).toSet
      val finalDfTyped = timeUsageGroupedTyped(timeUsageSummaryTyped(summaryDf)).toDF().rdd.collect().map(_.toString()).toSet
      assert(finalDf === finalDfSql)
      assert(finalDf === finalDfTyped)
    }
  }
}
